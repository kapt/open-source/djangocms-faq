from django.apps import AppConfig


class DjangocmsFaqConfig(AppConfig):
    name = "djangocms_faq"
