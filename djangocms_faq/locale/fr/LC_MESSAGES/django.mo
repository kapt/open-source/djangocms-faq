��          �      �        �   	  �   �     �     �     �     �  
   �  &   �     �     �  �        �     �  /   �  f        n     s  _   |  	   �     �     �  �   �  A  �  �   (  �   $	  	   
     $
     (
     9
     I
  #   W
     {
  	   �
    �
     �  "   �  ;   �  h   �     \     a  j   j     �     �     �    �              
      	                                                                                    <i>Editing a question using the django admin will only update the draft version, you will need to refresh the page to see the changes, and publish the page to made the changes appear on the live page.</i> <i>Editing the faq using the django admin will only update the draft page, you will need to refresh the page to see the changes, and publish the page to made the changes appear on the live page.</i> Draft Faq Faq Container Faq Question Faq Search Format: name (source page name).<br /> Keyword Keywords Keywords are not used for SEO purposes, but to allow visitors to find FAQ questions via synonyms. For example, for a question such as "Are pets allowed?", you could add the keyword "dog". Name No result found. Not on a page (maybe an apphook, like the blog) Optional. Title that will be displayed on top of the FAQ. If not specified, nothing will be displayed. Page Question Required. Will not be displayed on the website. Used to distinguish between several faqs.<br /> Search in Search name Title Unique slug for this question. Keep empty to let it be auto-generated. <br /><i>Warning: There is no control that the slug is unique, so you have to update it yourself if there is another question on the same page with the same slug.</i> Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 <i>Modifier une question en utilisant l'administration de django ne mettra à jour que la page de brouillon, vous devrez rafraîchir la page pour voir les changements, et publier la page pour que les changements apparaissent sur la page en direct.</i> <i>Modifier la faq en utilisant l'administration de django ne mettra à jour que la page de brouillon, vous devrez rafraîchir la page pour voir les changements, et publier la page pour que les changements apparaissent sur la page en direct.</i> Brouillon Faq Conteneur de Faq Question de Faq Recherche Faq Format: nom (nom de la page).<br /> Mot-clef Mot-clefs Les mot-clef ne sont pas utilisés à des fins de référencement, mais pour permettre aux visiteurs de trouver des questions de FAQ via des synonymes. Par exemple, pour une question "les animaux sont-ils acceptés", vous pouvez ajouter le mot-clef "chien". Nom Aucun résultat n'a été trouvé. Pas sur une page (peut-être sur un apphook, comme le blog) Facultatif. Titre qui sera affiché en haut de la FAQ. S'il n'est pas spécifié, rien ne sera affiché. Page Question Obligatoire. Ne sera pas affiché sur le site web. Utilisé pour s'y retrouver entre plusieurs faqs.<br /> Rechercher dans Nom de la recherche Titre Slug unique pour cette question. Laissez vide pour qu'il soit généré automatiquement. <br /><i>Attention : Il n'y a aucun contrôle que le slug est unique, vous devez donc le mettre à jour vous-même s'il y a une autre question sur la même page qui contient le même slug.</i> 