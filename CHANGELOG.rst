Changelog
=========


v1.5.1 (2022-09-26)
-------------------

Bug fixes
~~~~~~~~~
- Fix bugs with aliased faqs or questions [Corentin Bettiol]

  do not display aliased questions in results
  do not display questions in clipboard in results
  update base template (remove unused show_title test, display title if
  it exists


Documentation
~~~~~~~~~~~~~
- Update readme [Corentin Bettiol]
- Update changelog [Corentin Bettiol]


v1.5.0 (2022-07-28)
-------------------

Bug fixes
~~~~~~~~~
- Fix migration error in 0002 [Corentin Bettiol]

  Since no one use the old version of the app, I just removed the
  function that made the migration fail.

  I also updated the fuzzy search requirements (from fuzzywuzzy to
  thefuzz


Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v1.4.2 (2022-03-22)
-------------------

Bug fixes
~~~~~~~~~
- Show FAQ UUIDs in admin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [cb]

Maintenance
~~~~~~~~~~~
- Remove comments [cb]


v1.4.1 (2021-08-25)
-------------------

Bug fixes
~~~~~~~~~
- Remove uuid external package [cb]

  *(uuid is in python lib since at least python2.5


Documentation
~~~~~~~~~~~~~
- Update changelog [cb]


v1.4.0 (2021-08-25)
-------------------

Features
~~~~~~~~
- Add "name" field in FAQ [cb]

  *name is not displayed and is required, it helps to sort faqs
  *title is displayed and not required, it's just a title
  *added script to update uuids in migration 0003 (you may have same uuid for all faqs if you created faqs before applying migration 0002)
  *new help texts


Documentation
~~~~~~~~~~~~~
- Update changelog [cb]

Maintenance
~~~~~~~~~~~
- Update fr translations [cb]


v1.3.0 (2021-07-13)
-------------------

Features
~~~~~~~~
- Add MAX_QUERY_LENGTH setting [cb]

  * set a limit to the lenght of the requests submitted by the users (default: 60, use DJANGOCMS_FAQ_MAX_QUERY_LENGTH to change it

- Add option to improve results of api [cb]

  * add setting named DJANGOCMS_FAQ_SEARCH_WORD_BY_WORD_KEYWORDS, which will tell the module to search every word of the query in every word of each keyword for each questio


Bug fixes
~~~~~~~~~
- Process question through api only if len(question) > 2 [cb]

Documentation
~~~~~~~~~~~~~
- Update changelog [cb]


v1.2.1 (2021-06-02)
-------------------

Bug fixes
~~~~~~~~~
- Add default value if draft is not in api request [cb]

Documentation
~~~~~~~~~~~~~
- Update readme [cb]
- Update changelog [cb]

Maintenance
~~~~~~~~~~~
- Use camelCase for var names [cb]


v1.2.0 (2021-06-02)
-------------------

Features
~~~~~~~~
- Add django admin forms [cb]
- Add show_title (faq title) [cb]

  * CLOSE #

- Faq container titles are now optional [cb]

  *close #


Bug fixes
~~~~~~~~~
- Fix migration [cb]

  *set same uuid for draft & published version of Faq & Search instances (so search can search in draft & published faq using only one uuid

- Fix biggest bug of the project (yet) [cb]

  *when publishing a page where a faq is present, the cms plugin was recreated, and so the search_in id in the faq search plugin was deleted
  *I added a RelationSearchFaq class which store an uuid (that don't change each time the page is refreshed) for the faq and the search, and a small function which update the id in the relation table during after the faq plugin refresh
  *Relations between plugins. It is much harder to manage the copying of relations when they are from one plugin to another (https://docs.django-cms.org/en/latest/how_to/custom_plugins.html#relations-between-plugins)


Documentation
~~~~~~~~~~~~~
- Update changelog [cb]

Maintenance
~~~~~~~~~~~
- Typo in readme [cb]


v1.1.2 (2021-05-24)
-------------------

Documentation
~~~~~~~~~~~~~
- Typo + add fuzzywuzzy in requirements list [cb]
- Update changelog [cb]


v1.1.1 (2021-05-24)
-------------------

Documentation
~~~~~~~~~~~~~
- Remove useless str in README [cb]
- Update changelog [cb]


v1.1.0 (2021-05-24)
-------------------

Features
~~~~~~~~
- Add new setting [cb]

  *add DJANGOCMS_FAQ_FUZZY_SEARCH_PERCENTAGE setting
  *use DJANGOCMS_FAQ_FUZZY_SEARCH_PERCENTAGE value (default = 85) instead of fixed val
  *MAINT: update READM

- Try to add fuzzy search (experimental feature, not yet tested) [cb]

Documentation
~~~~~~~~~~~~~
- Add pip install command for git repo + optional dependencies [cb]
- Update changelog [cb]

Maintenance
~~~~~~~~~~~
- Run black & isort [cb]


v1.0.3 (2021-05-12)
-------------------

Bug fixes
~~~~~~~~~
- Add locale folder in release [cb]

Documentation
~~~~~~~~~~~~~
- Update changelog [cb]


v1.0.2 (2021-04-19)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [cb]

Maintenance
~~~~~~~~~~~
- Add locales (fr) [cb]


v1.0.1 (2021-04-07)
-------------------

Bug fixes
~~~~~~~~~
- Add missing templates [cb]

Documentation
~~~~~~~~~~~~~
- Update changelog [cb]


v1.0.0 (2021-04-07)
-------------------

Features
~~~~~~~~
- Link search_in to faqcontainer to search in faqsearchplugin [cb]
- Add settings SHOW_KEYWORDS_QUESTION and SHOW_KEYWORDS_ANSWER [cb]

  *update css of answers in faqsearchplugin to place keywords to the excteme right of the link bloc

- A question can now be inserted only on a faq container [cb]
- Add api ! [cb]

  * slightly update readme
  * format admin.py
  * add a new plugin named SearchFaqPlugin
  * add slug in question (autofill it on save if empty)
  *add searchfaqplugin model
  *add api url
  *add function (in utils.py) to get questions from a search string
  *add an "AnswerView" that is the api endpoint (return json)
  *recreate migration 0001
  *add unnecessary css in faq_question.html
  *add unnecessary css in faq_search.html
  *add simple js to open selected faq question
  *add not-so-simple js to use the api on a search plugi

- First commit! [cb]

  *add faq container plugin that can only contain faq question plugins
  *add faq question plugin that can only contain textplugin, fileplugin and videoplayerplugin by default, but we can override this in settings
  *add faq keywords object that can are linked to faq questions
  *add basic view
  *add license
  *add contributing guidelines
  *add admin form for creating new keywords
  *add first migration (may be recreated in a near future)
  *do not use in production until 1.0.0 is out


Bug fixes
~~~~~~~~~
- Faq ids were not copied when publishing a faqsearchplugin [cb]
- Fix bug in json view [cb]

  * add "no result found" notice on api when no result is foun


Documentation
~~~~~~~~~~~~~
- Improve readme [cb]
- Update readme (add small demo video) [cb]




.. Generated by gitchangelog
